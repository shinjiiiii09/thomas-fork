package com.cpsgroup.hypereton.sectioncounter.counter;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.cpsgroup.hypereton.sectioncounter.parser.PDFParser;

public class CounterTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator + "counter"
			+ File.separator;

	PDFParser parser;
	Counter counter;

	@Before
	public void init() {
		counter = new Counter("title descriptor", "closure descriptor",
				"[^b]section descriptor", "subsection descriptor");
	}

	@Test
	public void testCounter() {
		assertTrue(counter instanceof Counter);
	}

	@Test
	public void testGetTextStructure() {
		parser = new PDFParser();
		String content = "";
		try {
			content = parser.getContent(PATH_PREFIX
					+ "legalTextCompilation.pdf", 0, 0, true);
		} catch (IOException e) {
			fail("Exception thrown!");
		}
		int[] structureLevels = counter.getTextStructure(content);

		assertTrue("Expected 3 legal texts but got " + structureLevels[0]
				+ " instead.", structureLevels[0] == 3);
		assertTrue("Expected 13 sections but got " + structureLevels[1]
				+ " instead.", structureLevels[1] == 13);
		assertTrue("Expected 63 subsections but got " + structureLevels[2]
				+ " instead.", structureLevels[2] == 63);
	}
}
