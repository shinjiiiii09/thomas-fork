package com.cpsgroup.hypereton.sectioncounter;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

public class AppTest {

	private final String PATH_PREFIX = "src" + File.separator + "test"
			+ File.separator + "resources" + File.separator;
	private App app;

	@Before
	public void init() {
		app = new App();
	}

	@SuppressWarnings("static-access")
	@Test
	public void test() {
		assertTrue(app instanceof App);
		app.main(new String[] { "title descriptor",
				"closure descriptor", "[^b]section descriptor",
				"subsection descriptor", PATH_PREFIX + "invalid.pdf", "1", "1", "true" });
	}
}
