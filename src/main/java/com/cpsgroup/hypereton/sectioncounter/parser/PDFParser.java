package com.cpsgroup.hypereton.sectioncounter.parser;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.OverlappingFileLockException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFText2HTML;

/**
 * Provides methods to read in PDF files with text components and try to
 * preserve basic formatting by the use of HTML encoding.
 * 
 * 
 * @author Manuel Weidmann
 */
public class PDFParser {

	private final String QUOTE_START = "&#8222;";
	private final String QUOTE_END = "&#8220;";

	public PDFParser() {

	}

	/**
	 * 
	 * @param file
	 *            A {@link String} denoting the path to the PDF file which is to
	 *            be parsed.
	 * @param startingPage
	 *            The page from which to start parsing.
	 * @param endPage
	 *            The page after which to stop parsing.
	 * @param removeQuotes
	 *            Whether to remove all quoted text or not.
	 * @return An HTML encoded {@link String} containing the specified portion
	 *         of the PDF file.
	 * @throws IOException
	 *             If reading from the PDF file fails at any point.
	 * 
	 * @author Manuel Weidmann
	 */
	public String getContent(String file, int startingPage, int endPage,
			boolean removeQuotes) throws IOException {
		/**
		 * Create empty string to return at least a valid value even if no text
		 * was extracted.
		 */
		String content = "";

		/**
		 * Try to access specified file location.
		 */
		File in = new File(file);
		try {
			if (!in.exists()) {
				throw new IOException();
			}
		} catch (IOException e) {
			System.out.println("Error accessing " + file
					+ ", file does not exist!");
			throw e;
		}

		/**
		 * Lock file access to prevent concurrent modification, also check if
		 * file is already locked by another process.
		 */
		FileChannel channel = null;
		try {
			channel = new RandomAccessFile(file, "rw").getChannel();
			if (channel.tryLock() == null) {
				throw new IOException();
			}
		} catch (OverlappingFileLockException e) {
			System.out.println("Error accessing " + file + ", file is locked!");
			throw e;
		} catch (IOException e) {
			System.out.println("Error accessing " + file
					+ ", file can not be read from!");
			throw e;
		} finally {
			if (channel instanceof FileChannel) {
				channel.close();
			}
		}

		/**
		 * Let PDFbox ingest the file.
		 */
		PDDocument pdf;
		try {
			pdf = PDDocument.load(in);
		} catch (IOException e) {
			System.out.println("Error loading " + file
					+ ", not a valid PDF file!");
			throw e;
		}

		/**
		 * Prepare for text extraction: Create a 'to HTML' - text stripper; use
		 * UTF-encoding.
		 */
		PDFText2HTML stripper;
		try {
			stripper = new PDFText2HTML("utf-8");
		} catch (IOException e) {
			System.out.println("Error initialising text stripper!");
			throw e;
		}

		/**
		 * Perform some sanity checks on starting and end page inputs.
		 */
		if (startingPage > 0 && startingPage <= endPage
				&& startingPage <= pdf.getNumberOfPages()) {
			stripper.setStartPage(startingPage);
		}
		if (endPage > 0 && endPage >= startingPage
				&& endPage <= pdf.getNumberOfPages()) {
			stripper.setEndPage(endPage);
		}

		/**
		 * Store extracted text.
		 */
		try {
			content = stripper.getText(pdf);
		} catch (IOException e) {
			System.out.println("Error extracting text from" + file
					+ ", invalid doc state or file is encrypted!");
			throw e;
		}

		/**
		 * Close in-memory document.
		 */
		pdf.close();

		/**
		 * If the removal of quotes is requested, comply to do so.
		 */
		if (removeQuotes) {
			return getTextWithoutQuotes(content);
		} else {
			return content;
		}
	}

	/**
	 * 
	 * @param content
	 *            A {@link String} containing text with proper quotes, i.e. each
	 *            opening quotation mark is paired with a corresponding closing
	 *            one.
	 * @return The input {@link String} with all occurrences of quoted text
	 *         removed.
	 * 
	 * @author Manuel Weidmann
	 */
	private String getTextWithoutQuotes(String content) {
		/**
		 * Create Stringbuffer to write to and initialise open and close quote
		 * counts to 0
		 */
		StringBuffer buffer = new StringBuffer("");
		int openQuotes = 0;
		int quoteStart = 0;

		/**
		 * If the string is shorter than 6 characters it cannot contain an HTML
		 * encoded quotation mark; simply return the string in this case.
		 */
		if (content.length() < 7) {
			return content;
		}

		/**
		 * Read in the first 6 characters.
		 */
		for (int i = 0; i < 7; i++) {
			buffer.append(content.charAt(i));
		}

		/**
		 * Read in character after character and check for HTML encoded
		 * quotation marks. Count opening and closing ones, remove quotation
		 * marks and the text between first opening and corresponding closing
		 * one if such a pair is found.
		 */
		for (int i = 7; i < content.length(); i++) {
			buffer.append(content.charAt(i));

			if (buffer.substring(buffer.length() - 7, buffer.length()).equals(
					QUOTE_START)) {
				if (openQuotes == 0) {
					quoteStart = buffer.length() - 7;
				}
				openQuotes++;

			} else if (openQuotes > 0
					&& buffer.substring(buffer.length() - 7, buffer.length())
							.equals(QUOTE_END)) {
				openQuotes--;
				if (openQuotes == 0) {
					buffer.replace(quoteStart, buffer.length(), "");
				}
			}

		}

		return buffer.toString();
	}
}
