package com.cpsgroup.hypereton.sectioncounter;

//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileWriter;
//import java.io.IOException;
//
//import org.apache.pdfbox.pdmodel.PDDocument;
//import org.apache.pdfbox.util.PDFText2HTML;

public class App {

	// private static final String LAW_PREAMBLE =
	// "<b>Gesetz zur &#196;nderung ";
	// private static final String ACT_PREAMBLE = "<b>Verordnung ";
	// // class pattern !!!h
	// private static final String ARTICLE_1ST_LEVEL =
	// "<\\p{Lower}>Artikel \\d";
	// private static final String ARTICLE_2ND_LEVEL =
	// "<\\p{Lower}>\\d+\\p{Lower}?\\. ";
	//
	// private static final String SECTION_1ST_LEVEL = "<\\p{Lower}>&#167; \\d";
	// private static final String SECTION_2ND_LEVEL = "\\(\\d+\\) ";
	//
	// private static final String QUOTE_START = "&#8222;";
	// private static final String QUOTE_END = "&#8220;";

	public static void main(String[] args) {
		
		if (args.length < 5) {
			System.out.println("Too few arguments specified!");
			return;
		}
		
		try {
			new SectionCounter(args).run();
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

	// @Deprecated
	// public void countSections(String file, int startPage, int endPage) {
	// String content = getContent(file, startPage, endPage);
	//
	// String preamble, first_level, second_level;
	//
	// if (file.startsWith("gesetz")) {
	// preamble = LAW_PREAMBLE;
	// first_level = ARTICLE_1ST_LEVEL;
	// second_level = ARTICLE_2ND_LEVEL;
	// } else {
	// preamble = ACT_PREAMBLE;
	// first_level = SECTION_1ST_LEVEL;
	// second_level = SECTION_2ND_LEVEL;
	// }
	//
	// String[] laws = content.split(preamble);
	// String[] articles = content.split(first_level);
	// String[][] paragraphs = new String[articles.length][];
	//
	// for (int i = 1; i < articles.length; i++) {
	// paragraphs[i] = removeQuotedText(articles[i]).split(second_level);
	// }
	//
	// System.out
	// .println("Number of laws under revision:\t\t" + (laws.length));
	//
	// System.out.println("Number of first level sections:\t\t"
	// + (paragraphs.length - 1));
	//
	// System.out.print("Number of second level sections:\t");
	// int n = 0;
	// for (int i = 1; i < paragraphs.length; i++) {
	// n += paragraphs[i].length - 1;
	// }
	// System.out.println(n);
	//
	// }
	//
	// // öffnet pdf Datei, erstell Html Datei
	// // übergibt den Pfad der Html Datei als String
	//
	// /**
	// *
	// *
	// * @param file
	// * The path to the pdf file that is to be parsed.
	// * @param startingPage
	// * The starting page from which to start parsing.
	// * @param endingPage
	// * The ending page by which to stop parsing (including)
	// * @return An html encoded string comprised of the text portion of the
	// * specified pdf file.
	// */
	// @Deprecated
	// public String getContent(String file, int startingPage, int endingPage) {
	// File in, out;
	// FileWriter writer;
	// String content = "";
	//
	// try {
	// in = new File(file);
	// out = new File(file.replaceFirst("\\.pdf", "\\.html"));
	// out.createNewFile();
	// writer = new FileWriter(out);
	//
	// PDDocument pdf = PDDocument.load(in);
	//
	// PDFText2HTML stripper = new PDFText2HTML("utf-8");
	// if (startingPage > 0 && startingPage < pdf.getNumberOfPages()) {
	// stripper.setStartPage(startingPage);
	// }
	// if (endingPage >= startingPage
	// && endingPage <= pdf.getNumberOfPages()) {
	// stripper.setEndPage(endingPage);
	// }
	//
	// content = stripper.getText(pdf);
	//
	// writer.write(content);
	//
	// writer.close();
	// pdf.close();
	//
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// return content;
	// }
	//
	// /**
	// * @param content
	// * An html encoded string.
	// * @return The original string with all quoted text removed.
	// */
	// @Deprecated
	// private String removeQuotedText(String content) {
	// StringBuffer buffer = new StringBuffer("");
	// int openQuotes = 0;
	// int quoteStart = 0;
	//
	// for (int i = 0; i < 7; i++) {
	// buffer.append(content.charAt(i));
	// }
	//
	// for (int i = 7; i < content.length(); i++) {
	// buffer.append(content.charAt(i));
	//
	// if (buffer.substring(buffer.length() - 7, buffer.length()).equals(
	// QUOTE_START)) {
	// if (openQuotes == 0) {
	// quoteStart = buffer.length() - 7;
	// }
	// openQuotes++;
	//
	// } else if (buffer.substring(buffer.length() - 7, buffer.length())
	// .equals(QUOTE_END)) {
	// openQuotes--;
	// if (openQuotes == 0) {
	// buffer.replace(quoteStart, buffer.length(), "");
	// }
	// }
	//
	// }
	//
	// return buffer.toString();
	// }

}
