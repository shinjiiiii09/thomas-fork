package com.cpsgroup.hypereton.sectioncounter;

import java.io.IOException;

import com.cpsgroup.hypereton.sectioncounter.counter.Counter;
import com.cpsgroup.hypereton.sectioncounter.parser.PDFParser;

public class SectionCounter {

	private String file, titleDescriptor, closureDescriptor, sectionDescriptor, subsectionDescriptor;
	private int startingPage, endPage;
	private boolean removeQuotes;

	public SectionCounter(String[] args) 
		throws IllegalArgumentException {
		if (args.length < 5) {
			throw new IllegalArgumentException("Did not sufficiently specify input parameters!");
		}	
		
		file = args[4];
		startingPage = 0;
		endPage = 0;
		
		titleDescriptor = args[0];
		closureDescriptor = args[1];
		sectionDescriptor = args[2];
		subsectionDescriptor = args[3];

		try {
			if (args.length >= 7) {
				startingPage = Integer.parseInt(args[5]);
				endPage = Integer.parseInt(args[6]);
			}
			if (args.length == 6) {
				removeQuotes = Boolean.parseBoolean(args[5]);
			}
			if (args.length == 8) {
				removeQuotes = Boolean.parseBoolean(args[7]);
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Invalid arguments for page range; only positive non-zero integers allowed.");
		}
	}

	public boolean run() {
		PDFParser parser = new PDFParser();
		String content = "";

		try {

			content = parser.getContent(file, startingPage, endPage,
					removeQuotes);

			int[] structure = new Counter(titleDescriptor, closureDescriptor,
					sectionDescriptor, subsectionDescriptor)
					.getTextStructure(content);
			System.out.println("Number of legal texts:\t" + structure[0]);
			System.out.println("Number of sections:\t" + structure[1]);
			System.out.println("Number of subsections:\t" + structure[2]);

		} catch (IOException e) {
			System.out
					.println("Unable to access file. Make sure the path points to a valid unencrypted pdf file that is not locked by another program.");
			return false;
		}
		return true;
	}

}
